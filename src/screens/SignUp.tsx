/* eslint-disable no-lone-blocks */
import React from 'react';
import {
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  Alert,TextInput
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from '../components/Button';
import Input from '../components/Input';
import SignScaffold from '../components/SignScaffold';
import Tabs from './Tabs';
import { Formik } from 'formik';
import * as YUP from 'yup';

const FormValidationSchema =YUP.object().shape({
  username:YUP.string().min(8,"Too short").max(15).matches(
    /a-zA-Z0-9/,
     "Must Contain 6 Characters, two Uppercase, two Lowercase, twoNumber"
   ).required() ,
  email: YUP.string()
    .email("Invalid Email.")
    .required("Required"),
  password: YUP.string()
    .min(8, "Too Short!")
    .required("Required"),
  });


const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const logo = require('../../assets/images/logo-colour.png');

interface SignUpProps {
  navigation: any;
}

const SignUp = ({navigation}: SignUpProps) => {
  const behavior = Platform.OS === 'ios' ? 'padding' : undefined;

  const goToHome = () => {
    {
      /*

        #TASK

        Validate the signup form
          a. Username should have minimum 6 characters with 2 uppercase, 2 lowercase & 2 numbers atleast (eg: F22Labs)
          b. On successful validation, show a dropdown alert with green background saying `User name exists`
          c. On unsuccessful validation, show a dropdown alert with red background saying `Invalid username`
          d. Email should have basic email validation -completed
          e. Password should have minimum 8 characters -completed
          f. Show corresponding error messages below every field -completed

        NOTE: Making use of Formik & Yup libraries is preferred
      */
    }
    navigation.navigate(Tabs.name);
  };

  return (
    <SignScaffold>
      <Image style={styles.logo} source={logo} />
      <View style={styles.form}>
        <View>
          <Text style={styles.headerTitle}>Sign up</Text>
          <Text style={styles.headerSubtitle}>
            Enter your credentials to continue
          </Text>
        </View>
        <KeyboardAvoidingView behavior={behavior}>
          {/* <Input label="Username" />
          <View style={{marginTop: heightScreen * 0.011}} />
          <Input label="Email" />
          <View style={{marginTop: heightScreen * 0.011}} />
          <Input label="Password" /> */}


<Formik
        initialValues={{  username: "",email: "", password: "" }}
        validationSchema={FormValidationSchema}
        onSubmit={(values,actions)=> { 
         Alert.alert(JSON.stringify(values))}
        
        }
      >
        {props => {
          return (
            <View >
               <View style={styles.inputStyle}>
                <TextInput
                  placeholder="username"
                  value={props.values.username}
                  onChangeText={props.handleChange("username")}
                  onBlur={props.handleBlur("username")}
                />
              </View>
              <Text style={{ color: "red" }}>
                {props.touched.username && props.errors.username}
            </Text>
              <View style={styles.inputStyle}>
                <TextInput
                  placeholder="Email"
                  value={props.values.email}
                  onChangeText={props.handleChange("email")}
                  onBlur={props.handleBlur("email")}
                />
              </View>

              <Text style={{ color: "red" }}>
                {props.touched.email && props.errors.email}
              </Text>
              <View style={styles.inputStyle}>
                <TextInput
                  placeholder="Password"
                  value={props.values.password}
                  onChangeText={props.handleChange("password")}
                  onBlur={props.handleBlur("password")}
                  secureTextEntry
                />
              </View>

              <Text style={{ color: "red" ,  }}>
                {props.touched.password && props.errors.password }
                
              </Text>
             
            </View>
          );
        }}
      </Formik>
        </KeyboardAvoidingView>

        <View style={styles.termsBox}>
          <Text style={styles.infoText}>
            By continuing you agree to our{' '}
            <Text style={[styles.infoText, styles.greenInfoText]}>
              Terms of Service
            </Text>{' '}
            and{' '}
            <Text style={[styles.infoText, styles.greenInfoText]}>
              Privacy Policy
            </Text>
            .
          </Text>
        </View>
        <Button
          onPress={goToHome}
          bgColour="#53B175"
          txtColour="#FFF"
          text="Sign up"
        />
      </View>
    </SignScaffold>
  );
};

const styles = EStyleSheet.create({
  logo: {
    alignSelf: 'center',
    marginTop: heightScreen * 0.032,
    marginBottom: heightScreen * 0.112,
  },
  form: {
    paddingHorizontal: widthScreen * 0.06,
  },
  headerTitle: {
    fontSize: '1.625rem',
    lineHeight: '1.625rem',
    height: '1.625rem',
    fontFamily: '$gilroyNormal600',
    marginBottom: heightScreen * 0.017,
    color: '$blackColour',
  },
  headerSubtitle: {
    fontSize: '1rem',
    lineHeight: '1rem',
    height: '1rem',
    fontFamily: '$gilroyMedium',
    marginBottom: heightScreen * 0.045,
    color: '$darkGreyColour',
  },
  termsBox: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginBottom: heightScreen * 0.033,
  },
  infoText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: '$gilroyNormal600',
    fontWeight: '600',
    fontSize: '0.875rem',
    color: '$blackColour',
    letterSpacing: '0.05rem',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightScreen * 0.028,
  },
  greenInfoText: {
    color: '$greenColour',
    marginLeft: 5.0,
  },
});

export default {component: SignUp, name: 'SignUp'};

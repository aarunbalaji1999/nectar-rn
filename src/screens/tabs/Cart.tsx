import React,{useState,useEffect} from 'react';
import {View, Text, Dimensions,ScrollView,FlatList} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import FoodCard from '../../components/FoodCard';
import OrderAccepted from '../status/OrderAccepted';
import Button from '../../components/Button';



const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const {width: screenWidth, height: screenHeight} = Dimensions.get('screen');

interface AccountProps {
  navigation: any;
}
interface FoodCardProps {
  title: any;
  desc: string;
  price: string;
  imageUrl: string;
}


const CartTab = ( {navigation}: AccountProps) => {

  const OrderAccepted = ({title,desc,price,imageUrl}:FoodCardProps) => {
    navigation.navigate(OrderAccepted.name);
  };

  const [items,setitems]=useState([])
  const Oredered = async()=>{
  
    const result =  await fetch('https://jsonblob.com/api/jsonBlob/b5e57bc9-cf9e-11eb-a671-8b3a1cc0f833',
 
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
  
    setitems(result)
    console.log("**"+JSON.stringify(result))
   
}
useEffect(() => {
  Oredered()
 
     return () => {
         console.log("clean up")
     };
 }, [])

const renderitem = (items:any) =>{
  console.log("*12"+JSON.stringify(items))
 

  return(
    <>
    <FoodCard title={items.title} />
    <FoodCard desc={items.desc} />
    <FoodCard price={items.price} />
    <FoodCard imageUrl={items.image} />
    </>
 
  )
  }
  return (
    <>
    <View style={styles.container}>
      {/*

        #TASK

        Create the UI for cart screen
          a. Fetch the list of cart items from endPoint available in helpers/Endpoints.ts
          b. Refer prints folder cart screen UI & replicate it as such.
          c. Changing the quantity & removing the item functionality should be implemented.
          c. The screen should be scrollable if there are a lot of items on cart.
          d. Clicking on `Go to checkout` should take user to OrderAccepted screen. Refer /screens/status/OrderAccepted.tsx & prints/screen_order_accepted.png
          e. Handle `no items in cart` UI too

      */}
      
      <Text>My Cart</Text>

      <View>
     
        <FoodCard
        data={items}
        keyExtractor={(items)=>items.id}
        
        renderItem={({items})=>renderitem(items)}
        
      
        ></FoodCard>
      

      </View>
     
      <View style={styles.buttonBox}>
      <Button
        onPress={OrderAccepted}
        text="OrderAccepted"
        bgColour="#53B175"
        txtColour="#FFF"
      
      />
    </View>
    </View>
     
    </>
  );
};

const styles = EStyleSheet.create({
  container: {
    width: widthScreen,
    minHeight: heightScreen,
    paddingTop: 35.0,
    backgroundColor: '$whiteColour',
    display: 'flex',
    alignItems: 'center',
  },
  buttonBox: {
    paddingBottom: screenHeight * 0.027,
    paddingHorizontal: screenWidth * 0.06,
    
  
  },
});

export default {component: CartTab, name: 'Cart'};

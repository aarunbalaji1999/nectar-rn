import React,{useState} from 'react';
import { SafeAreaView,ScrollView, View, Text, TouchableOpacity,Image, Dimensions, Platform,
  PermissionsAndroid,StyleSheet} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProfileImage from '../../../assets/images/profile.png';
import {AccountIcons} from '../../helpers/Icons';
import AccountListItem from '../../components/AccountListItem';
import Button from '../../components/Button';
import Onboarding from '../Onboarding';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';


const {width: screenWidth, height: screenHeight} = Dimensions.get('screen');

interface AccountProps {
  navigation: any;
}

const AccountTab = ({navigation}: AccountProps) => {
  const [filePath, setFilePath] = useState({});

  const itemList = [
    {
      label: 'My Details',
      icon: (
        <AccountIcons.PersonalCardIcon style={styles.icon} color={'#181725'} />
      ),
    },
    {
      label: 'Orders',
      icon: <AccountIcons.OrdersIcon style={styles.icon} color={'#181725'} />,
    },
    {
      label: 'Delivery Address',
      icon: <AccountIcons.PinIcon style={styles.icon} color={'#181725'} />,
    },
    {
      label: 'Payment Methods',
      icon: <AccountIcons.PaymentIcon color={'#181725'} />,
    },
    {
      label: 'Notifications',
      icon: <AccountIcons.BellIcon color={'#181725'} />,
    },
    {
      label: 'About',
      icon: <AccountIcons.AboutIcon color={'#181725'} />,
    },
  ];

  const goToOnboarding = () => {
    navigation.navigate(Onboarding.name);
  };
 

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async () => {
    let options = await{
      mediaType: "photo",
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
   
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        console.log('base64 -> ', response.base64);
        console.log('uri -> ', response.uri);
        console.log('width -> ', response.width);
        console.log('height -> ', response.height);
        console.log('fileSize -> ', response.fileSize);
        console.log('type -> ', response.type);
        console.log('fileName -> ', response.fileName);
        setFilePath(response);
      });
    }
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: "photo",
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary({options}, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      // console.log('base64 -> ', response.base64);
      // console.log('uri -> ', response.uri);
      // console.log('width -> ', response.width);
      // console.log('height -> ', response.height);
      // console.log('fileSize -> ', response.fileSize);
      // console.log('type -> ', response.type);
      // console.log('fileName -> ', response.fileName);
      setFilePath(response);
    });
  };


  return (
    <>
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.headerContainer}>
          {/*

            #TASK

            Integrate image picker
              a. Ask user permission to access camera/media usage
              b. If permission denied, show alert if they want to close it & take them to settings to enable it***
              c. Picker to restrict only images
              d. Show preview of the selected image

          */}

          <Image style={styles.headerImage} source={ProfileImage} />
          <View style={styles.textBox}>
            <View style={styles.headerTitleBox}>
              <Text style={styles.headerTitle}>F22Labs</Text>
            </View>
            <Text style={styles.headerSubtitle}>test@f22labs.com</Text>
          </View>
          
        </View>
        
        <View >
        {/* <Image
          source={{
            uri: 'data:image/jpeg;base64,' + filePath.data,
          }}
          style={styles.imageStyle}
        /> */}
        <Image
          source={{uri: filePath.uri}}
          
        />
         <Text style={styles.textStyle}>{filePath.uri}</Text>
         {/* <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={() => captureImage()}>
          <Text style={styles.textStyle}>
            Launch Camera for Image
          </Text>
        </TouchableOpacity> */}
          <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={() => chooseFile('photo')}>
          <Text style={styles.textStyle}>Choose Image</Text>
        </TouchableOpacity>
        </View>
        
        <View style={styles.list}>
          {itemList.map((item, index) => {
            return (
              <AccountListItem
                key={index}
                label={item.label}
                children={item.icon}
              />
            );
          })}
        </View>
        <View style={styles.buttonBox}>
          <Button
            onPress={goToOnboarding}
            text="Log Out"
            bgColour="#F2F3F2"
            txtColour="#53B175"
          />
        </View>
      </ScrollView>
    </>
  );
};

const styles = EStyleSheet.create({
  scrollContainer: {
    backgroundColor: '$whiteColour',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: screenWidth * 0.06,
    paddingTop: screenHeight * 0.1,
    paddingBottom: screenHeight * 0.033,
    borderBottomWidth: 1.0,
    borderBottomColor: '$lightGreyColour',
  },
  headerImage: {
    width: screenHeight * 0.171,
    height: screenHeight * 0.171,
    borderRadius: screenHeight * 0.171,
  },
  textBox: {
    marginLeft: screenWidth * 0.05,
  },
  headerTitleBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTitle: {
    fontFamily: '$gilroyNormal600',
    fontWeight: '600',
    fontSize: '1.125rem',
    color: '$blackColour',
    marginRight: screenWidth * 0.024,
  },
  headerSubtitle: {
    fontFamily: '$gilroyNormal',
    fontSize: '0.875rem',
    color: '$darkGreyColour',
  },
  headerIcon: {
    color: '$greenColour',
  },
  list: {
    paddingBottom: screenHeight * 0.058,
  },
  buttonBox: {
    paddingBottom: screenHeight * 0.027,
    paddingHorizontal: screenWidth * 0.06,
  },
  icon: {
    color: '#181725',
  },
});

export default {component: AccountTab, name: 'Account'};

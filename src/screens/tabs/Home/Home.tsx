import React,{useState,useEffect} from 'react';
import {View, ScrollView, Dimensions, Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import LogoColour from '../../../../assets/images/logo-colour.png';
import Banner from '../../../../assets/images/home_screen/banner.png';
import SearchBar from '../../../components/SearchBar';
import SectionTitle from './SectionTitle';
import FoodCard from '../../../components/FoodCard';


const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const Home = () => {
  const [items,setitems]=useState([])
  const api = async()=>{
  
    const result =  await fetch('https://jsonblob.com/api/jsonBlob/bc938c99-cf7d-11eb-a671-3b544f13a561',
 
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
  
    setitems(result)
    console.log("**"+JSON.stringify(result))
   
}
useEffect(() => {
  api()
 
     return () => {
         console.log("clean up")
     };
 }, [])

const renderitem = (items:any) =>{

  return(
    <FoodCard>
            <FoodCard title={items.title} />
            <FoodCard desc={items.desc} />
            <FoodCard price={items.price} />
            <FoodCard imageUrl={items.image} />
            
           </FoodCard>
  )
  }
  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <Image source={LogoColour} style={styles.logo} />
      </View>
      <View style={[styles.localBox, styles.searchBox]}>
        <SearchBar />
      </View>
      <View style={styles.localBox}>
        <Image style={styles.banner} source={Banner} />
      </View>

      {/*

        #TASK

        Integrate API
          a. Endpoints available at helpers/endPoints.ts
          b. Replace the static piece of code with data from API

      */}
      <View >
        
      </View>
      <View style={styles.localBox}>
        <SectionTitle title="Exclusive Offer" />
      </View>
      <ScrollView style={styles.horizontalScroll} horizontal={true}>
        <FoodCard
        data={items}
        renderItem={({items})=>renderitem(items)}
        onRefresh={() => {
          
          api()
         
      }}
        ></FoodCard>
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
      </ScrollView>
      <View style={styles.localBox}>
        <SectionTitle title="Best Selling" />
      </View>
      <ScrollView style={styles.horizontalScroll} horizontal={true}>
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
        <FoodCard
          title="Tomato"
          desc="1kg"
          price="4.49"
          imageUrl="https://images.unsplash.com/photo-1576856497337-4f2be24683da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1882&q=80"
        />
      </ScrollView>
      <View style={styles.scrollFooter} />
    </ScrollView>
  );
};

const styles = EStyleSheet.create({
  localBox: {
    paddingHorizontal: 25.0,
  },
  container: {
    width: widthScreen,
    minHeight: heightScreen,
    paddingTop: 35.0,
    backgroundColor: '$whiteColour',
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
    width: 36.48,
    height: 40.0,
  },
  searchBox: {
    marginTop: 20.0,
  },
  banner: {
    marginTop: 20.0,
    width: widthScreen * 0.87,
    resizeMode: 'contain',
  },
  horizontalScroll: {
    paddingLeft: 20.0,
    paddingBottom: 10.0,
  },
  scrollFooter: {
    marginBottom: heightScreen * 0.2,
    backgroundColor: 'red',
  },
});

export default {component: Home, name: 'Home'};

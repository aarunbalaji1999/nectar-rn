# F22Labs React Native Challenge
## Welcome! 👋

- The challenge is for a total of 40 points. 
- There are 4 tasks in total & each task has 10 points attached to them. 
- Have a look through the entire codebase to understand each requirement & complete them.
- We have made use of TypeScript, try to work with it. If you find it difficult, feel free to declare as `any` type wherever you face issue.
## Working on the Solution

To clone and run this application, you'll need [Git](https://git-scm.com/downloads) and [React Native](https://reactnative.dev/docs/getting-started) installed on your computer.

- Clone the repo in your local machine & search for `#TASK`  keyword to start the challenge.

- Create your own `private repo`. Push the changes that you work to the repo created by you.

- Be `CAUTIOUS`, do not push to the task repo.


From your command line:

```
# Install dependencies.
$ yarn install


# Install pods
$ cd ios && pod install && cd ../

# Run the IOS app
$ yarn ios

# Run the android app
# yarn android
```
### Note
- Check if the IOS simulator is selected.
- Create a local.properties file inside android folder if the SDK is not found. [Reference](https://stackoverflow.com/a/48155800/6081801)

### If you are new to TYPESCRIPT, feel free to add `// @ts-nocheck` at the top of the file that you are working on & continue using JS.

## Sharing your solution

- Add us as a collaborator in your repo. We can share our git handle once you complete the task.
### Android Build
```
$ cd android && ./gradlew assembleRelease

# APK found inside /android/app/build/outputs/apk/release/app-release.apk
```
### IOS Build
```
Refer https://stackoverflow.com/a/42130914/6081801 for step by step instructions.
```
- Get the working build and share it to us via [diawi](https://www.diawi.com/)

**Have fun building!**  🚀
